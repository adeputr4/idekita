@extends('admin.layouts.main')

@section('header')
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block">POSTINGAN</a>
@endsection


@section('content')
<div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="mb-0">Postingan Idea</h3>
                    </div>
                    <div class="col-md-4 ">
                        <button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#addIdea">Add Idea</button>
                    </div>
                </div>
              
             </div>
            <div class="table-responsive">
            <form method="post">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">TITLE</th>
                    <th scope="col">AUTHOR</th>
                    <th scope="col">STATUS</th>
                    <th scope="col">TAKEN BY</th>
                    <th scope="col">OPTIONS</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($ideas as $idea)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        
                        <div class="media-body">
                          <a href="#"><span class="mb-0 text-sm">{{$idea->title}}</span></a>
                        </div>
                      </div>
                    </th>
                    <td>
                        {{$idea->username}}
                    </td>
                    <td> <!-- USER -->
                      <span class="badge badge-dot mr-4">
                            @switch($idea->status_ide)
                                @case(0)
                                <button type="button" class="btn btn-danger btn-sm" border-radius="10px">Unverified</button>                  
                                @break
                                @case(1)
                                <button type="button" class="btn btn-success btn-sm" border-radius="10px">Verified</button>         
                                @break
                                @case(2)
                                <button type="button" class="btn btn-success btn-sm" border-radius="10px">Taken</button>         
                                @break
                            @endswitch
                      </span>
                    </td>
                    <td> <!-- STATUS -->
                      <div class="avatar-group">
                           @if(!empty($idea->taken_by))
                           <a href="https://wa.me/{{$idea->taken_by}}" target="_blank">
                           {{$idea->taken_by}}</a>
                           @else
                        
                           @endif

                      </div>
                    </td>
                    <td> <!-- OPTIONS -->
                      <div class="d-flex align-items-center">
                        @if($idea->status == -1)
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#verif{{$idea->id}}" disabled >Verification</button>
                        @else
                            @switch($idea->status_ide)
                                @case(0)
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#verif{{$idea->id}}" >Verification</button>
                                    @break
                                @case(1)
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#takenBy{{$idea->id}}">Take it</button>
                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#unverif{{$idea->id}}">Unverification</button>
                                    @break
                                @case(2)
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#takenBy{{$idea->id}}" disabled>Taken</button>
                                    @break 
                            @endswitch
                        @endif
                        
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteNow{{$idea->id}}">Delete</button>
                         </div>
                    </td>
                  </tr>
                  @endforeach
                  
                </tbody>
              </table>
            </div>
            
          </div>
        </div>

    
@endsection

            @foreach($ideas as $idea)
                <!-- Ban Data -->
                <div class="modal fade" id="verif{{$idea->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                            </div>
                            <form action="{{route('admin.ideas.updateStatusIde',[$idea->id,$idea->status])}}" method="POST">
                                <div class="modal-body">
                                    Are you sure want to Verification <strong>{{$idea->title}}?</strong>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="{{$idea->id}}">
                                        <input type="hidden" name="status_ide" value="1">
                                        <button type="submit" class="btn btn-link waves-effect">YES</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                        @csrf
                                        <input type="hidden" name="_method" value="PUT">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach

            @foreach($ideas as $idea)
                <!-- Ban Data -->
                <div class="modal fade" id="unverif{{$idea->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                            </div>
                            <form action="{{route('admin.ideas.updateStatusIde',[$idea->id,$idea->status])}}" method="POST">
                                <div class="modal-body">
                                    Are you sure want to Unverification <strong>{{$idea->title}}?</strong>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="{{$idea->id}}">
                                        <input type="hidden" name="status_ide" value="0">
                                        <button type="submit" class="btn btn-link waves-effect">YES</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                        @csrf
                                        <input type="hidden" name="_method" value="PUT">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach

            @foreach($ideas as $idea)
                <!-- Delete Data -->
                <div class="modal fade" id="deleteNow{{$idea->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                            </div>
                            <form action="{{route('admin.ideas.destroy',[$idea->id])}}" method="POST">
                                <div class="modal-body">
                                    Are you sure want to Unverification <strong>{{$idea->title}}?</strong>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="{{$idea->id}}">
                                        <button type="submit" class="btn btn-link waves-effect">YES</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach


                <div class="modal fade" id="addIdea" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Add Idea</h4>
                            </div>
                            <form action="{{route('admin.ideas.store')}}" method="POST">
                                <div class="modal-body">
                                <form method="post">
                                <input name="title" placeholder="title" class="form-control" placeholder="Title" type="text">
                                    <br/>
                                    <textarea name="content" id="mytextarea" placeholader="Write your briliant ideas"></textarea>
                                    <br />
                                    @csrf
                                    <input type="hidden" name="status_ide" value="1">
                                    <input type="hidden" name="user_id" value="{{Auth::user()->id }}">
                                    <button type="submit" class="btn btn-primary waves-effect">Post</button>
                                </form>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


 @foreach($ideas as $idea)
                <!-- taken Data -->
                <div class="modal fade" id="takenBy{{$idea->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                            </div>
                            <form action="{{route('admin.ideas.updateStatusIde',[$idea->id,$idea->status])}}" method="POST">
                                <div class="modal-body">
                                <label for="taken_by">Phone Number</label>
                                <input name="taken_by" class="form-control" placeholder="ex: 628xxx" type="text">
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="{{$idea->id}}">
                                        <input type="hidden" name="status_ide" value="2">
                                        <button type="submit" class="btn btn-link waves-effect">YES</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                        @csrf
                                        <input type="hidden" name="_method" value="PUT">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach


            