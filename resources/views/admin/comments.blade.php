@extends('admin.layouts.main')

@section('header')
<a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block">COMMENTS</a>
@endsection


@section('content')
	<div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Comments</h3>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Comments</th>
                    <th scope="col">Ideas</th>
                    <th scope="col">USER</th>
                    <th scope="col">STATUS</th>
                    <th scope="col">OPTIONS</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($comments as $comment)
                    <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        
                        <div class="media-body">
                          <span class="mb-0 text-sm">{{$comment->comment}}</span>
                        </div>
                      </div>
                    </th>
                    <td>
                        <a href="#">{{ str_limit($comment->title, 10) }}</a>
                    </td>
                    <td> <!-- USER -->
                      <span class="badge badge-dot mr-4">
                         {{$comment->username}}
                      </span>
                    </td>
                    <td> <!-- COMMENTS -->
                      <div class="avatar-group">
                      @switch($comment->status_comment)
                                @case(0)
                                <button type="button" class="btn btn-warning btn-sm" border-radius="10px">Not Displayed</button>                  
                                @break
                                @case(1)
                                <button type="button" class="btn btn-success btn-sm" border-radius="10px">Displayed</button>      
                                @break
                            @endswitch

                      </div>
                    </td>
                    <td> <!-- OPTIONS -->
                      <div class="d-flex align-items-center">
                      @switch($comment->status_comment)
                                @case(0)
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#verif{{$comment->id}}" >Verification</button>
                                    @break
                                @case(1)
                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#unverif{{$comment->id}}">Unverification</button>
                                    @break  
                            @endswitch
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteNow{{$comment->id}}">Delete</button>
                      </div>
                    </td>
                  </tr>
                    @endforeach
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>

        	

            <!-- Ban Data -->
            <div class="modal fade" id="banNow" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                        </div>
                       	<form>
                        <div class="modal-body">
                            Are you sure want to ban this data?
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-link waves-effect">BAN</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                <input type="hidden" name="_method" value="GET">
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection

 @foreach($comments as $comment)
                <!-- Ban Data -->
                <div class="modal fade" id="verif{{$comment->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                            </div>
                            <form action="{{route('admin.comments.updateStatus',[$comment->id,$comment->status])}}" method="POST">
                                <div class="modal-body">
                                    Are you sure want to Verification <strong>{{$comment->title}}?</strong>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="{{$comment->id}}">
                                        <input type="hidden" name="status_comment" value="1">
                                        <button type="submit" class="btn btn-link waves-effect">YES</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                        @csrf
                                        <input type="hidden" name="_method" value="PUT">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach

            @foreach($comments as $comment)
                <!-- Ban Data -->
                <div class="modal fade" id="unverif{{$comment->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                            </div>
                            <form action="{{route('admin.comments.updateStatus',[$comment->id,$comment->status_comment])}}" method="POST">
                                <div class="modal-body">
                                    Are you sure want to Unverification <strong>{{$comment->title}}?</strong>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="{{$comment->id}}">
                                        <input type="hidden" name="status_comment" value="0">
                                        <button type="submit" class="btn btn-link waves-effect">YES</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                        @csrf
                                        <input type="hidden" name="_method" value="PUT">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach

            @foreach($comments as $comment)
                <!-- Delete Data -->
                <div class="modal fade" id="deleteNow{{$comment->id}}" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Edit Data</h4>
                            </div>
                            <form action="{{route('admin.comments.destroy',[$comment->id])}}" method="POST">
                                <div class="modal-body">
                                    Are you sure want to delete <strong>{{$comment->comment}}?</strong>
                                    <div class="modal-footer">
                                        <input type="hidden" name="id" value="{{$comment->id}}">
                                        <button type="submit" class="btn btn-link waves-effect">YES</button>
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach